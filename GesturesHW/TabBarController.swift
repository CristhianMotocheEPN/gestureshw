//
//  TabBarController.swift
//  GesturesHW
//
//  Created by Cristhian Motoche on 4/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewDidAppear(_ animated: Bool) {
        let items = self.tabBar.items
        
        items?[0].title = "Pinch"
        items?[1].title = "Long Press"
    }
}
