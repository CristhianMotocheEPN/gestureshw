//
//  PinchViewController.swift
//  GesturesHW
//
//  Created by Cristhian Motoche on 4/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit
import Hue

class PinchViewController: UIViewController {

    // Controllers
    @IBOutlet var pinchOnView: UIPinchGestureRecognizer!
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var barProgressView: UIProgressView!
    @IBOutlet weak var scaleSliderView: UISlider!
    @IBOutlet weak var velocitySliderView: UISlider!

    var progress:Float = 0.1

    // UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        self.progress = self.barProgressView.progress
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func pinchOnViewMaker(_ sender: UIPinchGestureRecognizer) {
        updateNavigationBarColor(sender.scale, sender.velocity)
    }

    // Sliders
    @IBAction func scaleSlideAction(_ sender: UISlider) {
        let val = sender.value
        self.pinchOnView.scale = CGFloat(val)
    }

    // Update Color
    fileprivate func updateNavigationBarColor(_ scale: CGFloat, _ velocity: CGFloat) {
        let desaturatedBlue = UIColor(hex: "#aaaacc")
        print("Scale \(scale)")
        print("Velocity \(velocity)")
        let saturatedBlue = desaturatedBlue.addHue(1.3, saturation: scale, brightness: scale, alpha: abs(velocity))
        let randomColor = desaturatedBlue.addHue(velocity, saturation: scale, brightness: scale, alpha: abs(velocity))
        // Box View
        self.boxView.backgroundColor = saturatedBlue
        
        // Bar Progress
        self.barProgressView.backgroundColor = randomColor
        if self.barProgressView.progress == 1.0 {
            self.progress = 0.0
            self.barProgressView.progress = 0.0
        }
        self.progress = self.progress + Float(abs(scale))
        self.barProgressView.progress = self.progress
    }
}
