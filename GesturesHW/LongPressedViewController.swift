//
//  LongPressedViewController.swift
//  GesturesHW
//
//  Created by Cristhian Motoche on 4/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class LongPressedViewController: UIViewController {

    // Outlets
    @IBOutlet var longPressGesture: UILongPressGestureRecognizer!
    @IBOutlet weak var boxView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Slider changes
    @IBAction func tapRequiredChangeValueAction(_ sender: UISegmentedControl) {
        self.longPressGesture.numberOfTapsRequired = sender.selectedSegmentIndex
    }
    @IBAction func touchesRequiredChangeValueAction(_ sender: UISegmentedControl) {
        self.longPressGesture.numberOfTouchesRequired = sender.selectedSegmentIndex + 1
    }

    @IBAction func pressDurationAction(_ sender: UISlider) {
        self.longPressGesture.minimumPressDuration = CFTimeInterval(sender.value)
    }
    
    @IBAction func movementSpaceAction(_ sender: UISlider) {
        self.longPressGesture.allowableMovement = CGFloat(sender.value)
    }
    
    // On Long Pressed
    @IBAction func longPressedAction(_ sender: UILongPressGestureRecognizer) {
        // Logging
        print("Taps: \(sender.numberOfTapsRequired)")
        print("Touches: \(sender.numberOfTouchesRequired)")
        print("Duration: \(sender.minimumPressDuration)")
        print("Allow Movement: \(sender.allowableMovement)")

        // Change color
        let desaturatedBlue = UIColor(hex: "#aaaacc")
        let randomColor = desaturatedBlue.addHue(CGFloat(sender.minimumPressDuration), saturation: CGFloat(sender.numberOfTapsRequired), brightness: sender.allowableMovement, alpha: CGFloat(sender.numberOfTouchesRequired))

        // Box View
        self.boxView.backgroundColor = randomColor
    }
}
